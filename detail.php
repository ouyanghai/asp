    <?php
$refer = isset($_GET['refer']) ? $_GET['refer'] : ( isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "" );
$from = isset($_GET['from']) ? $_GET['from'] : "";
$id = isset($_GET['id']) ? $_GET['id'] : "";
$sid = isset($_GET['sid']) ? $_GET['sid'] : "";
$iid = isset($_GET['iid']) ? $_GET['iid'] : "";
$cid = isset($_GET['cid']) ? $_GET['cid'] : 0;
$did = isset($_GET['did']) ? $_GET['did'] : 0;//detail_id
$forward = "";

if( $from == "tbd" ){
    $url = parse_url($refer);
    $query = isset($url['query']) ? $url['query'] : "";
    parse_str($query, $params);

    $word = "";
    isset($params['wd']) && $word = urldecode($params['wd']);
    isset($params['word']) && $word = urldecode($params['word']);

    
    if( empty($id) && empty($sid)){
        require_once("./asp.php");
        exit;
    }
    else{
        $url = "http://i.haodianpu.com/best/bdtg/tongji?id={$id}&did={$did}&sid={$sid}&rdm=".time();
    }

    $route = @file_get_contents( $url );
    empty($route) && $route = "http://e.ytx.la/";
    $route .= ( strstr($route, "?") == "" ? "?" : "&" ) . "from=tg"; 
    if( strstr($route, "shop") ) {            
        if( preg_match('/shop([\d]{5,12})\.taobao\.com/', $route, $match)) {
            $sid = $match[1];
        }
    }
    if( strstr($route, "item.taobao.com") ) {          
        if( preg_match('/id=([\d]{9,12})\S+(?:\.0|&sid=(\d{6,12}))/', $route, $match)) {
            $iid = $match[1];
			$sid = !empty($match[2]) ? $match[2] : '';
        }
    }  
    if( !empty($refer) && preg_match("/(www\.baidu\.com|taobao\.com)/i", $refer) ){
        $ip = !empty($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : ( !empty($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : ( !empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "127.0.0.1" ) );
        $url = "http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
        $content = @file_get_contents( $url );
        if( !empty($content) && substr($content, 0, 1) == "{" && substr($content, -1, 1) == "}" ){
            $json = json_decode($content);
            if( $json->code == 0 && $json->data->city != "\u5317\u4eac\u5e02" ){
                $forward = $route;
            }
        }
    }
}
if ( empty($sid) && empty($iid) ) {    
    include_once("./tejia.php");
    exit;
}

include_once( "./header.php" );
?>

<div id="show-item" class="container">
    <div class="item-detail">
        <div class="clearfix">
            <div class="item-pic">
                <a href="" target="_blank"><img src=""></a>
            </div>
            <div class="item-info">
                <h3><a href="" target="_blank" id="item-title"></a></h3>
                <h3>卖家昵称:<span id="item-nick"></span></h3>
                <div class="item-buy">
                    <i style="color:#ff6600"><span style="color:black">价格:</span>￥</i>
                    <span class="item-price"></span>
                    <a class="pte_buy" href="" target="_blank"></a>
                </div>
                <span id="item-goshop"><a style="color:#fff" target="_blank" href="">去淘宝抢购</a></span>
            </div>
        </div>
    </div>
    
</div>
<div id="item-navs">
    <div id="item-menu">
        <span style="background:red;display:inline-block;height:35px;line-height:35px;color:white;">商品分类</span>
        <div id="item-cate"></div>
    </div>
    <!--显示宝贝描述或分类下的宝贝-->
    <div id="item-description"></div>
</div>
<div id="show-shop" class="container">
    <div class="clearfix" style="height:120px;">
        <div class="shop-pic">
            <a href="" target="_blank"><img src=""></a>
        </div>
        <div class="shop-info">
            <div class="shop-detail">
                <p><a href="" target="_blank"><span class="detail1" style="color:red;font-size:15px;font-weight:bold"></span></a></p>
                <p><strong>店铺简介：</strong><span class="detail2"></span></p>
                <p><strong>综合评分：</strong><span class="detail4">4.8</span></p>
                <p><strong>开店时间：</strong><span class="detail3"></span></p>
            </div>
             <!-- JiaThis Button BEGIN -->
            <div class="jiathis_style_32x32" style="float:right;margin-top:50px;margin-right:40px;">
                <a href="" target="_blank" id="shop-title" style="float:left;margin-right:5px;width:127px;height:36px;background:url('http://img03.taobaocdn.com/imgextra/i3/62192401/T2RzaxXodbXXXXXXXX_!!62192401.gif') no-repeat 95% center;display:block;"></a>
                <a class="jiathis_button_qzone"></a>
                <a class="jiathis_button_tsina"></a>
                <a class="jiathis_button_weixin"></a>
            </div>
            <script type="text/javascript" src="http://v3.jiathis.com/code_mini/jia.js?uid=1373890231172857" charset="utf-8"></script>
            <!-- JiaThis Button END -->
        </div>

    </div>
</div>
<!--
<div style="width:980px;margin:0 auto;margin-top:20px;height:32px;color:#ff6600;"><span style="font-size:20px;float:left;"><img src="./images/asp/remai01.png"></span></div>
-->
<!--店铺宝贝展示-->

<div class="shop-items">
</div>

<script type="tpl" id="template">
    <dl class="item-row">
        <div class="items-flag"></div>
        <a href="http://item.taobao.com/item.htm?id={{iid}}" target="_blank">
        <dt class="item-row-pic"><img src="{{pic_url}}" title="{{title}}"></dt>
        <dd class="item-row-desc" title="{{title}}">{{title}}</dd>
        <dd class="item-row-price" title="{{price}}"><span>{{price}}</span></dd>
        </a>
    </dl>
</script>

<script type="text/javascript">

$(function(){
    var s = document.createElement( 'script' );    
    //进入首页
    <?php if(!empty($sid) && empty($iid)) { ?>
        s.src = "/api.php?sid=<?php echo $sid; ?>&callback=shop&ts=" + (+new Date());
    <?php 
    //进入宝贝页,待做
	} else if(!empty($sid) && !empty($iid)) { 
		$from = (!empty($from) && $from=='tbd') ? '&from=tbd' : '';
	?>
        //s.src = "/api.php?sid=<?php echo $sid; ?>&callback=show&ts=" + (+new Date());
        s.src = "/api.php?iid=<?php echo $iid; ?>&sid=<?php echo $sid;?>&cid=<?php echo $cid;?>&callback=show<?php echo $from;?>&ts=" + (+new Date());    
    <?php } ?>    
    
    document.body.appendChild( s );
    
    <?php if( !empty($forward) ) { ?>
    //setTimeout(function(){self.location="<?php echo $forward;?>";}, 5000);
    <?php } ?> 


});

// 展示宝贝
function show(json){
    console.log(json);
    json.detail_url = json.detail_url + ( json.detail_url.search(/\?/) > -1 ? "&" : "?" ) + "from=tg";    
    $("#show-item,#item-navs").show();
    $("#item-title").text( json.title ).attr( "href", json.detail_url );
    $("#item-nick").text(json.nick);
    $(".item-pic").find("a").attr("href", json.detail_url);
    $("#item-goshop").find("a").attr("href",json.detail_url);
    if (json.pic_url != '') {
        $(".item-pic").find("img").attr("src", json.pic_url);
    }
    $("#item-description").html( json.desc );
    $("#item-cate").html(json.cate);
    $(".item-price").text( json.price );
    $(".pte_buy").attr("href", json.detail_url).hide();

    /*
    if (json.items) {
        for (var i in json.items) {
            var html = $("#template").html();
            for (var j in json.items[i]) {
                reg = "/{{" + j + "}}/ig";
                html = html.replace( eval( reg ), json.items[i][j] );
            }
            $(".shop-items").append(html);
        }
    }
    */
    document.title = json.title;

    $("#item-cate li.optgroup span").click(function(){
        var ul = $(this).parent().find("ul");
        var i = $(this).find("i");
        if(ul.css("display") == "block"){
            ul.css("display","none");
            i.removeClass("item-down").addClass("item-up"); 
        }else{
            ul.css("display","block");
            i.removeClass("item-up").addClass("item-down");
        }
        
    });
    $("#item-cate li.lival").click(function(){
        var val = $(this).val();
        window.location.href="./detail.php?from=tbd&id="+<?php echo $id; ?>+"&sid="+<?php echo $sid; ?>+"&cid="+ val;
    });
}

// 展示店铺信息
function shop(json){
    json.url = json.url + ( json.url.search(/\?/) > -1 ? "&" : "?" ) + "from=tg";
    $("#show-shop").show();
    $("#shop-title").attr("href", "<?php echo $route; ?>");
    $(".shop-pic").find("a").attr("href", json.url);
    $(".shop-detail").find("a").attr("href", json.url);
    json.pic_path = json.pic_path.replace(" ", "");
    if (json.pic_path != "") {
        $(".shop-pic").find("img").attr("src", 'http://logo.taobao.com/shop-logo'+json.pic_path);
    }
    $(".shop-detail .detail1").text(json.title);
    $(".shop-detail .detail2").html(json.desc);
    $(".shop-detail .detail3").text(json.created);
    var _height = parseInt($(".shop-info").height()+30);
    if (_height>300) {
        $("#show-shop").height(_height);
    }
    
    if (json.items) {
        for (var i in json.items) {
            var html = $("#template").html();
            for (var j in json.items[i]) {
                reg = "/{{" + j + "}}/ig";
                html = html.replace( eval( reg ), json.items[i][j] );
            }
            $(".shop-items").append(html);
        }
    }
}
</script>
<?php include_once("./footer.php"); ?>