<?php include_once( "./header.php" ); ?>

<div class="container">
    <dl style="padding:10px 50px 50px 50px;">
        
        <dt style="font-size:18px; font-weight:bold; margin-top:20px;color:#777777">公司简介</dt>
        <dd style="text-indent:2em; font-size:12px;color:#777777"><p>爱搜品网是深圳营天下云计算有限公司旗下购物类导航网站。每日精选服饰、鞋包、母婴、居家等众多优质特卖商品,欢迎选购!</p></dd>

<?php if( $domain == "haodianpu.com"){?>

        <dt style="font-size:18px; font-weight:bold; margin-top:20px;color:#777777">经营理念</dt>
        <dd style="text-indent:2em; font-size:12px;color:#777777">专注 极致 口碑 快</dd>

        <dt style="font-size:18px; font-weight:bold; margin-top:20px;color:#777777">主营业务</dt>
        <dd style="text-indent:2em; font-size:12px;color:#777777"><a href="http://fuwu.taobao.com/serv/shop_index.htm?page_id=293424&isv_id=695972963&page_rank=2&tab_type=1" target="_blank" style="color:#FF0000;">淘宝ISV >>> </a></dd>

<!--        
        <dt style="font-size:18px; font-weight:bold; margin-top:20px;">主要产品</dt>
        <dd style="font-size:14px;">
            <div style="clear:both;height:80px;padding:20px;"><a href="http://fuwu.taobao.com/ser/detail.htm?service_code=ts-1810960&tracelog=search&from=home" target="_blank"><img align="left" src="http://img01.taobaocdn.com/top/i1/T1oUE6XndcXXaCwpjX.png" style="margin-right:20px;" border="0"></a><b>淘流量_超强推广引流</b><br><br>一键推广，超强引流，点击不再收费。独创一键通过广告联盟，快速投放在上百优质站点，快速超强引流，率先实现流量来源可追溯。</div>
            <p align="center"><img src="http://img03.taobaocdn.com/imgextra/i3/62192401/T2XZT_XdxXXXXXXXXX-62192401.gif"></p>
            <p align="center"><img src="http://img03.taobaocdn.com/imgextra/i3/62192401/T28EdZXstaXXXXXXXX-62192401.gif"></p>
            <p align="center"><img src="http://img03.taobaocdn.com/imgextra/i3/62192401/T2y7jhXn4aXXXXXXXX-62192401.gif"></p>
            <p align="center"><img src="http://img04.taobaocdn.com/imgextra/i4/62192401/T2hoN1XspaXXXXXXXX-62192401.gif"></p>
            <p align="center"><img src="http://img04.taobaocdn.com/imgextra/i4/62192401/T2v4l7XdldXXXXXXXX-62192401.gif"></p>
        </dd>
   
   
        <dt style="font-size:18px; font-weight:bold; margin-top:20px;">主要产品</dt>
        <dd style="font-size:14px;">
            <div style="clear:both;height:80px;padding:20px;"><a href="http://fuwu.taobao.com/ser/detail.htm?service_code=ts-13461&tracelog=search&from=home" target="_blank"><img align="left" src="http://img01.taobaocdn.com/top/i1/T1GvuDFddfXXb1upjX.jpg" style="margin-right:20px;" border="0"></a><b>终极营销 -- 提升销量、客单价、转化率、好评</b><br><br>淘宝卖家打折促销工具，包含促销打折（任意折扣）/团购/满就送、减现金、包邮、送礼物、送优惠劵/ 关联营销、关联模板/好评送优惠劵/回头客送优惠劵/一键微博分享/自动上架/批量修改等功能。</div>
            <p align="center"><img src="http://img04.taobaocdn.com/imgextra/i4/62192401/T2tN2JXaBbXXXXXXXX_!!62192401.jpg"></p>
        </dd>
-->
<?php }else{?>
        <!--
        <dt style="font-size:18px; font-weight:bold; margin-top:20px;color:#777777">产品介绍</dt>
        <dd style="text-indent:2em; font-size:12px;color:#777777">
            公司从事软件产品的研发与销售，主要是电子商务相关的运营管理等类软件。为从事电子商务的大小卖家提供好用的软件工具。
            <table width="100%" height="140"  border="0"  cellpadding="15" cellspacing="15">
            <tr align="center" valign="middle">
            <td>
            <img src="http://img01.taobaocdn.com/top/i1/T1oOVLXehyXXaCwpjX.png"><br />促销专家_限时打折_优惠券营销
            </td>
            <td>
            <img src="http://img01.taobaocdn.com/top/i1/T1L8iUFexaXXb1upjX.jpg"><br />天天特卖_限时打折_发布手机详情
            </td>
            <td>
            <img src="https://img.alicdn.com/imgextra/i4/62192401/TB2OELXqFXXXXXyXXXXXXXXXXXX_!!62192401.png"><br />推广王_专注淘宝中小卖家推广
            </td>
            </tr>
            </table>            
        </dd>
        -->
<?php }?>

        <dt style="font-size:18px; font-weight:bold; margin-top:20px;color:#777777">联系方式</dt>
        <dd style="text-indent:2em; font-size:12px;color:#777777">
            <p><strong>电话：</strong><?php echo $contact;?></p>
            <p><strong>地址：</strong><?php echo $address;?></p>
        </dd>

    </dl>
</div>

<?php include_once("./footer.php"); ?>