<?php include_once( "./header.php" ); ?>
<!--用于关键字搜索-->
<input id="file_path" type="hidden" value="4"/>
<link rel="stylesheet" type="text/css" href="./css/shop.css">

<div class="content">
    <div class="shop-items"></div>
    <div class="page-items">
        <div class="tcdPageCode"></div>
    </div>
</div>

<script type="tpl" id="template">
    <div class="shop-row">
        <div class="items-shop">
            <span class="shop-img"><a href="http://shop{{sid}}.taobao.com" target="_blank"><img src="http://logo.taobao.com/shop/{{pic}}" /></a>
            </span>
            <span class="shop-title">
                <a href="http://shop{{sid}}.taobao.com" target="_blank"> {{title}}</a>
            </span>
            <span class="shop-level"><a href="http://shop{{sid}}.taobao.com" target="_blank">{{level}}</a></span><br/>
            <font color="#666">卖家:</font>
            <span class="shop-nick">
            <a href="http://shop{{sid}}.taobao.com" target="_blank">{{nick}}</a>
            <a target="_blank" href="http://www.taobao.com/webww/ww.php?ver=3&touid={{nick}}
&siteid=cntaobao&status=2&charset=utf-8"><img style="border:0;width:16px;height:16px;" src="http://amos.alicdn.com/online.aw?v=2&uid={{nick}}&site=cntaobao&s=2&charset=utf-8" alt="点这里给我发消息" /></a>
            </span><br/>
            
            <span class="shop-cat"><font color="#666">主营:</font><a title="{{cat}}" href="http://shop{{sid}}.taobao.com" target="_blank">{{cat}}</a></span>
        </div>
        <ul class="bao-items">
            <li>
                <a href="http://item.taobao.com/item.htm?id={{iid0}}" target="_blank"><img style="width:140px;height:140px;" src="{{pic_url0}}"/></a>
                <span class="bao-item-price">￥{{price0}}</span>
            </li>
            <li>
                <a href="http://item.taobao.com/item.htm?id={{iid1}}" target="_blank"><img style="width:140px;height:140px;" src="{{pic_url1}}"/></a>
                <span class="bao-item-price">￥{{price1}}</span>
            </li>
            <li>
                <a href="http://item.taobao.com/item.htm?id={{iid2}}" target="_blank"><img style="width:140px;height:140px;" src="{{pic_url2}}"/></a>
                <span class="bao-item-price">￥{{price2}}</span>
            </li>
            <li>
                <a href="http://item.taobao.com/item.htm?id={{iid3}}" target="_blank"><img style="width:140px;height:140px;" src="{{pic_url3}}"/></a>
                <span class="bao-item-price">￥{{price3}}</span>
            </li>
        </ul>
        <span style="float:right;margin-top:10px;margin-right:20px;"><a style="color:#0063dc" href="http://shop{{sid}}.taobao.com" target="_blank">查看更多宝贝&gt;</a></span>
    </div>
</script>

<?php 
$page=!empty($_GET['page'])?$_GET['page']:1; 
$t_page = !empty($_GET['keyword'])?1:10;
$keyword = !empty($_GET['keyword'])?$_GET['keyword']:'';
?>
<script type="text/javascript">

$(function(){
    var s = document.createElement( 'script' );
    s.src = "/api.php?hpos=1&page=<?php echo $page; ?>&keyword=<?php echo $keyword; ?>&callback=shop&ts=" + (+new Date());
    document.body.appendChild( s );
});
function shop(json){
    if (json.items) {
        for (var i in json.items) {
            var html = $("#template").html();
            for (var j in json.items[i]) {
                for(var k in json.items[i]['shops']){
                    for(var h in json.items[i]['shops'][k]){
                        reg = "/{{" + h +k+ "}}/ig";
                        html = html.replace( eval( reg ), json.items[i]['shops'][k][h] ); 
                    }
                }
                if(j != 'shops'){
                    reg = "/{{" + j + "}}/ig";
                    if(j == 'level'){
                        var pindex = Math.ceil(parseInt(json.items[i][j])/5);
                        var ind = parseInt(json.items[i][j])%5;
                        ind = ind==0?5:ind;
                        var thtml="<i class='start_"+pindex+"'></i>";
                        var phtml = '';
                        for(var index=1; index<=ind;index++){
                            phtml += "<i class='start_"+pindex+"'></i>"
                        }
                        console.log(phtml);
                        html = html.replace( eval( reg ), phtml );
                    }else{
                        html = html.replace( eval( reg ), json.items[i][j] );        
                    }
                }
                
            }
            $(".shop-items").append(html);
        }
    }else{
        $("#nothing_tip").show();
    }
    var pages = 3;
    if(json.pages){
        pages = json.pages;
    }
    $(".tcdPageCode").createPage({
        pageCount:pages,
        current:<?php echo $page; ?>,
        backFn:function(p){
            location.href = "./shop.php?page="+p;
        }
    });
}

</script>
<?php include_once( "./footer.php" ); ?>