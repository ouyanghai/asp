/**
 * 图片头数据加载就绪事件 - 更快获取图片尺寸
 * @version	2011.05.27
 * @author	TangBin
 * @see		http://www.planeart.cn/?p=1121
 * @param	{String}	图片路径
 * @param	{Function}	尺寸就绪
 * @param	{Function}	加载完毕 (可选)
 * @param	{Function}	加载错误 (可选)
 */
var imgReady = (function () {
    var list = [], intervalId = null,

    // 用来执行队列
    tick = function () {
        var i = 0;
        for (; i < list.length; i++) {
            list[i].end ? list.splice(i--, 1) : list[i]();
        };
        !list.length && stop();
    },

    // 停止所有定时器队列
    stop = function () {
        clearInterval(intervalId);
        intervalId = null;
    };

    return function ( jsonObj, url, length , i, el , newBox , childClass, ready, load, error) {

        var onready, width, height, newWidth, newHeight, img = new Image();

        img.src = url+'_190x190.jpg';

        // 如果图片被缓存，则直接返回缓存数据
        if (img.complete) {
            //ready.call(img);
            //load && load.call(img);
            //return;
        };

        width = img.width;
        height = img.height;
        img.el = el ;
        img.newBox = newBox ;
        img.childClass = childClass ;
        img.i = i ;
        img.length = length ;
        // 加载错误后的事件
        img.onerror = function () {
            error && error.call(img);
            onready.end = true;
            img = img.onload = img.onerror = null;
        };

        // 图片尺寸就绪
        onready = function () {
            newWidth = img.width;
            newHeight = img.height;
            if (newWidth !== width || newHeight !== height ||
                // 如果图片已经在其他地方加载可使用面积检测
                newWidth * newHeight > 1024
            ) {
                //alert(ready)
                ready.call(img);
                onready.end = true;
            };
        };
        //onready();

        // 完全加载完毕的事件
        img.onload = function () {
            // onload在定时器时间差范围内可能比onready快
            // 这里进行检查并保证onready优先执行
            !onready.end && onready();

            load && load.call(img);

            // IE gif动画会循环执行onload，置空onload即可
            img = img.onload = img.onerror = null;
        };

        // 加入队列中定期执行
        if (!onready.end) {
            list.push(onready);
            // 无论何时只允许出现一个定时器，减少浏览器性能损耗
            if (intervalId === null) intervalId = setInterval(tick, 40);
        };
    };
})();

/**
 * 瀑布流
 **/
var t = 0;              // 全局变量，重排开关。服务器能够给出图片宽高则不需要此项
var startH = 0;         // 加载元素的初始位置
var warpWidth = 220;    // 格子宽度
var innerWidth = 190;   // 内宽度
var margin = 15;        // 格子间距
var isAjax = false;     // ajax开关
var sumChild = 0;       // 记录当前总共有多少个格子
var h = [];             // 记录每列的高度
var scoll_init = false;

var page = 1;
var data_finish = false;

var tpl;

var waterfall_id   = 'waterfall';       // 要展示的位置
var waterfall_item = 'item';            // 要区块的 class
var waterfall_loading = 'wf_loading';   // 加载中 id
var waterfall_more = 'wf_more';         // 更多

var _loading;
var more;

var _el, _childClass;

function sortNew(el,newBox){
    var box = newBox;
    postPosition(el,box,"add");//执行定位函数
    for(var i = 0; i < box.length; i++) {
            box[i].style.visibility = "visible"; //定位完毕后显示新增节点
    }
    startH = h[0];
//    isAjax=0;


    isAjax = false;

    if( navigator.userAgent.indexOf("Firefox") > -1 && !scoll_init )
    {
        scoll_init = true;
        scroll( 0, 0 );
//        scroll(0,startH-700);//火狐滚动条问题，暂无好的解决办法，随便打个补丁先
    }
    
    if ( _loading && _loading != undefined ) {
        _loading.style.display = 'none';
    }
}
function sortAll(el,childTagName){
    h = []; //每次重排都要重置列高度记录数组

    // var box = el.getElementsByTagName(childTagName);

    var allElements = el.getElementsByTagName( 'div' ); 

    var box = [];
    var n = 0;

    for (var i=0; i< allElements.length; i++ ) 
    { 
        var classNames = allElements[i].className.split(' ');

        for (var j = 0; j < classNames.length; j++) {
            if ( classNames[j] == childTagName ) {
                box[n] = allElements[i];
                n++;
            }
        }
    } 

    postPosition( el, box, "re" );  // 执行定位函数
}

function postPosition(el,box,op){
    
    var minH = box[0].offsetHeight,
    boxW = box[0].offsetWidth + margin;

    n = el.offsetWidth / boxW | 0;
    
    if( typeof document.body.style.maxHeight === "undefined" ) {
        el.style.width = n * boxW - margin + "px";
    }
    
    el.style.visibility = "visible";
    
    for(var i = 0; i < box.length; i++) {   // 排序算法，有待完善
        
        boxh = box[i].offsetHeight;         // 获取每个Pin的高度
        if(i < n && op =="re" || (i < n && op =="add" && h.length < n ) ) { // 第一行特殊处理
            h[i] = boxh;
            box[i].style.top = 0 + 'px';
            box[i].style.left = (i * boxW) + 'px';
            box[i].style.opacity = 1;
        } else { 
            minH = Array.min(h); //取得各列累计高度最低的一列
            minKey = getarraykey(h, minH);
            h[minKey] += boxh+margin ; //加上新高度后更新高度值
            box[i].style.top = minH+margin + 'px';
            box[i].style.left = (minKey * boxW) + 'px';
            box[i].style.opacity = 1;
        }
    }
    
    maxH = Array.max(h); 
    maxKey = getarraykey(h, maxH);
    el.style.height = h[maxKey] +"px";
}

Array.min = function(array)
{
    return Math.min.apply( Math,array );
}
Array.max = function(array)
{
    return Math.max.apply( Math,array );
}
/* 返回数组中某一值的对应项数 */
function getarraykey( s, v ) {
    for( k in s ) {
        if( s[k] == v ) {
            return k;
        }
    }
}
function getNumber(){
    return Math.floor(document.documentElement.clientWidth / ( warpWidth + margin ));
}
    
function getMore(el,childClass){
    
    _el = el;
    _childClass = childClass;
    
    if ( data_finish || isAjax ) {
        return false;
    }
    isAjax = true;

    if ( _loading && _loading != undefined ) {
        _loading.style.display = 'inline-block';
    }
    
    var n = getNumber();
    var s = document.createElement( 'script' );
    s.src = "/api.php?action=get&callback=stateChange&num=" + 3 * n + "&page=" + page + "&s=" + Math.random();
    
    document.body.appendChild( s );
}

function stateChange( data ){
    changeJeson( _el, data, _childClass );    // 处理jeson
}
    
function changeJeson(el,json,childClass){
    
    var jsonString = json; // 获取服务器返回的json字符串
    var jsonObj = null;
    
    if ( typeof( json ) == 'object' ) {
        jsonObj = json;
    } else {
        try {
            jsonObj = eval('(' + jsonString + ')'); // 将json字符串转换成对象
        } catch (ex) {
            getMore( el, waterfall_item );
            return null;
        }
    }
    
    if ( page>=50 ) {
        data_finish = true;
        
        if ( more && more != undefined ) {
            more.style.display = 'inline-block';
        }
    }
    
    page++;
    
    addList(el,jsonObj,childClass);//执行添加节点函数
}
function addList(el,jsonObj,childClass){
    
    var newBox = [];
    
    sumChild = sumChild + Number( jsonObj.length );
    
    for(var i=0;i<jsonObj.length;i++)
    {	
        var src =String(jsonObj[i].src);
        //var width =String(jsonObj.images[i].width);
        //var height =String(jsonObj.images[i].height);
        //$id("aaa1").innerHTML =$id("aaa1").innerHTML + src+","
        
//        jsonObj['content'][i]['pic_url'] += "_220x220.jpg";
        
        imgReady( jsonObj[i], jsonObj[i]['pic_url'], jsonObj.length , i , el , newBox , childClass ,function () {
            
            var height = this.height;
            var src = this.src;
            var el = this.el;
            var newBox = this.newBox;
            var childClass = this.childClass;
            var i = this.i;
            var length = this.length;

            jsonObj[i].height = height;

            callBackAdd( jsonObj[i], height, src , length, i ,el,newBox,childClass);
            
        });
    }
    
    //chackImg(el,newBox);
}

function callBackAdd( jsonObj, height, src, length, i ,el,newBox,childClass){
    
    var div = document.createElement("div");
    div.className = childClass + " " + "popup_in";
    
    var _html = $id( 'tpl' ).innerHTML;
    
    jsonObj['warpWidth'] = warpWidth;
    jsonObj['innerWidth'] = innerWidth;
    
    if ( jsonObj['remark'] == undefined ) {
        jsonObj['remark'] = '';
    }
    
    if ( jsonObj['remark'] ) {
        jsonObj['hidden'] = '';
    } else {
        jsonObj['hidden'] = 'hidden';
    }

    for ( var j in jsonObj ) {
        var reg = "/{{" + j + "}}/ig";
        _html = _html.replace( eval( reg ), jsonObj[ j ] );
    }
    
    div.innerHTML = _html;
    
    div.style.width = warpWidth +"px";
    div.style.top = startH +"px";
    div.style.opacity = 0;
    el.appendChild(div);
    newBox[i]=div;
    t++;
    
    if( t > length -1 ){
        sortNew(el,newBox);
        t=0;
    }
}

function chackImg(el,newBox){
    var imgs =[];
    imgs=el.getElementsByTagName("img");
    for(i=sumChild;i<imgs.length;i++){
        //var img = new SImage(icall,el,imgs.length,imgs[i].src,newBox);
    }
}

window.onload = function() {
//    scroll(0,0);

    _loading = $id( waterfall_loading );
    innerWidth = warpWidth - margin * 2;
    
    if ( document.getElementById( waterfall_more ) ) {
        more = $id( waterfall_more );
    }
    
    _el = $id( waterfall_id );
    
    getMore( _el, waterfall_item );
    return;
};

var re;
var so;
window.onresize = function() {
    
    if( typeof document.body.style.maxHeight === "undefined" ) {
        var _width = document.documentElement.offsetWidth;
        if ( _width > 1428 + 12 ) {
            _el.style.width = 1428;
        } else {
             if ( _width > 25 ) {
                 _width = _width - 25;
             }
            _el.style.width = _width;
        }
    }
    
    clearTimeout(re);
    re = setTimeout( resize, 100 );
}

function resize(){
    $id( waterfall_id ).className = "waterfall active";
    sortAll( $id( waterfall_id ), waterfall_item );
}

window.onscroll = function(){
    
    if ( data_finish || isAjax ) {
        return;
    }
    
    var a = document.documentElement.scrollTop == 0 ? document.body.clientHeight : document.documentElement.clientHeight;
    var b = document.documentElement.scrollTop == 0 ? document.body.scrollTop : document.documentElement.scrollTop;
    var c = document.documentElement.scrollTop == 0 ? document.body.scrollHeight : document.documentElement.scrollHeight;

    if( b > 0 && a + b + 20 >= c && !isAjax ){
        setTimeout(function(){
            getMore( $id( waterfall_id ), waterfall_item );
        }, 100 );
    } else {
        return;
    }
}