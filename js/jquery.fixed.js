/*!
 * jQuery Fixed Plugin
 * version: 0.06.0-2013.06.26
 * @requires jQuery v1.5 or later
 * Copyright (c) 2013 Jenvan
 * Examples and documentation at: http://pithy.cn/jquery/fixed/
 * Project repository: https://github.com/jenvan/jqueryfixed
 * Dual licensed under the MIT and GPL licenses.
 * https://github.com/jenvan/jqueryfixed#copyright-and-license
 */

;(function($){
"use strict";

/*
    Usage Note:
    -----------

    $(document).ready(function() {
        $('#toolbar').fixed({ 
			x: [ "" | number | "left" | "right" | "center" ],
			y: [ "" | number | "top" | "bottom" | "middle" ],
			force: [ true | false ],
			debug: [ true | false ]
		});
    });

*/


var defaults = {
    x: "",
	y: "",
    force: false,
	debug: false	
};


$.fn.fixed = function( options ) {
    
    var settings = $.extend( {}, defaults, options );
	
	$.fn.fixed.debug = settings.debug;
    
    return this.each(function() {
        
		var obj = $(this);
		
		// 对象的原始 定位方式、尺寸和位置
		var op = $(obj).css("position");		
		var ow = $(obj).outerWidth();
		var oh = $(obj).outerHeight();
		var ox = $(obj).offset().left;
		var oy = $(obj).offset().top;       

		$(window).bind("load scroll resize orientationchange", function(e) {

			// 窗口的尺寸
			var sw = $(window).width();
			var sh = $(window).height();
			var sx = $(document).scrollLeft();
			var sy = $(document).scrollTop();

			
			// 计算出变量对应的相关位置
			var p = {
				left: 0,
				right: sw - ow,
				top: 0,
				bottom: sh - oh,
				center: parseInt( ( sw - ow ) / 2 ),
				middle: parseInt( ( sh - oh ) / 2 )
			};

				
			// 解析参数中的位置
			var x = settings.x !== "" ? settings.x : ox;
			var y = settings.y !== "" ? settings.y : oy;
			
			if( $.inArray(x, ["top", "bottom", "middle"] ) > -1 ){
				var temp = x;
				x = y;
				y = temp;
			}

			x = typeof(p[x]) !== "undefined" ? p[x] : parseInt(x);
			y = typeof(p[y]) !== "undefined" ? p[y] : parseInt(y);			

			if( x < 0 )
				x = x + sw - ow;
			if( y < 0 )
				y = y + sh - oh;

			
			//log(ox, "|", oy);
			//log(sx, "|", sy);
			log(x, "|", y);


			// 如果不强制浮动，即超过边界时才开始浮动
			if( settings.force !== true && (  ( ox <= x && oy <= y ) || ( ox >= sx && oy >= sy ) ) )
				return $(obj).css( { position: op, left: ox, top: oy } );


			// 执行浮动
			if( ! ( navigator.userAgent.match(/\bMSIE (4|5|6)\./) || navigator.userAgent.match(/\bOS (3|4|5|6)_/) || navigator.userAgent.match(/\bAndroid (1|2|3|4)\./i) ) )
				return $(obj).css( { position: "fixed", left: x, top: y } );			
			return $(obj).css( { position: "absolute", left: x + sx, top: y + sy } );
		
		});	
        
    });

};


$.fn.fixed.debug = false;


function log(){
	if( !$.fn.fixed.debug )
		return;	
    var msg = '[jquery.fixed] ' + Array.prototype.join.call(arguments, ' ');
    if( window.console && window.console.log )
        window.console.log(msg);
    else if( window.opera && window.opera.postError )
        window.opera.postError(msg);
	else
		window.status = msg;
}


})(jQuery);
