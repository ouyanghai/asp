<?php
function curl( $url, $postFields=null, $show=false ){
    // 1.初始化
    $ch = curl_init();
    
    // 2. 设置选项
    curl_setopt($ch, CURLOPT_URL, $url);            // 目标网址
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 返回结果，而不输出
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // 支持重定向
    curl_setopt($ch, CURLOPT_REFERER, "http://www.taobao.com");//伪造来路（我是从淘宝过来的，不要屏蔽我啊）
    
    if( $show )
        curl_setopt($ch, CURLOPT_HEADER, true);     // 显示头部信息
    
    // SSL加密
    if(substr($url,0,5)=="https"){
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    }
    
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
    curl_setopt($ch, CURLOPT_FAILONERROR, false);       // 显示HTTP状态码
    
    // 传参
    if( is_array($postFields) && count($postFields)>0 ){
        $postBodyString = "";
        $postMultipart = false;
        
        foreach($postFields as $k => $v){
            //判断是不是文件上传
            if("@" != substr($v, 0, 1)) {
                $postBodyString .= "$k=" . urlencode($v) . "&"; 
            }
            else{
                //文件上传用multipart/form-data，否则用www-form-urlencoded(表单提交)
                $postMultipart = true;
            }
        }
        unset($k, $v);
        
        curl_setopt($ch, CURLOPT_POST, true);
        if( $postMultipart ){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        }
        else{
            curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString,0,-1));
        }
    }
    
    // 3. 执行并获取HTML文档内容
    $output = curl_exec($ch);
    
    // 检错
    if( curl_errno($ch) ){
        throw new Exception( curl_error($ch), 0 );                
    }
    else{
        $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (200 !== $httpStatusCode){
            //throw new Exception($reponse,$httpStatusCode);
        }
    }
    
    // 4. 释放curl句柄
    curl_close($ch);
    
    return $output;
}
?>