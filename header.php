<?php
    $info = array(
        "tuitao.org" => array("深圳市兰瑞电子商务有限公司", "0755-61315919", "深圳市南山区桂庙路光彩新天地公寓16B6", "京ICP备06018896号"),
        "shbike.com.cn" => array("深圳牛人软件有限公司", "0755-29972979", "深圳市宝安区鸿隆广场B3座817", "沪ICP备12025716号-1"),
        "51yingxiao.com.cn" => array("深圳微告传媒有限公司", "0755-36522048", "深圳市宝安区西乡街道河东大厦612", "京ICP备13031424号-1"),
		"ytx.la" => array("爱搜品-优质商品导购 为您挑选好商品 楚楚街，卷皮网，9.9元包邮，天天特价，蘑菇街，折800", " 0755-27678161", "宝安区宝安25区华丰青贸大厦501", "粤ICP备15056724号"),
    );

    $host = $_SERVER["HTTP_HOST"];
    $domain = preg_replace("/([^\.]+)\.([^\.]+)(\.[com\.cn|net\.cn|org\.cn|com|net|org|cn|la])/iU", "\$2\$3", $host);    
    if( !isset($info[$domain]) )
        exit("ERROR: ".strtoupper($domain));
    $company = $info[$domain][0];
    $contact = $info[$domain][1];
    $address = $info[$domain][2];
    $beian = $info[$domain][3];
    $req = $_SERVER['PHP_SELF'];
    if(strpos($req, 'remai.php')){
    	$company = "爱搜品-爱搜品热卖单品top100-热卖推荐";
    }else if(strpos($req, 'tejia.php')){
    	$company = "爱搜品-爱搜品天天特价!9.9元特价包邮，九块九包邮";
    }else if(strpos($req, 'shop.php')){
    	$company = "爱搜品-精选淘宝天猫好店推荐_最具性价比店铺";
    }
    /*
    //上头条
    else if(strpos(, '')){
    	$company .= " 淘友分享网购经验_购物全攻略";
    }
    */
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $company;?></title>
    <link href="/css/basic.css" rel="stylesheet" type="text/css" />
    <link href="/css/waterfall.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://libs.baidu.com/jquery/1.7.2/jquery.js"></script>
    <script type="text/javascript" src="/js/common.js?ver=20130228"></script>
    <script type="text/javascript" src="/js/waterfall.js?ver=20130228"></script>
    <script type="text/javascript" src="/js/jquery.fixed.js?ver=20130625"></script>
    <script type="text/javascript" src="/js/asp.js?ver=20130625"></script>
	<link rel="stylesheet" type="text/css" href="./css/asp.css">
</head>
<body>
	<div class="header">
		<div class="background"></div>
		<div class="area">
			<div class="area_in">
				<span><a href="">商家入口</a>|<a href="./about.php">关于我们</a></span>
			</div>
			<div class="area_logo">
				<span class="area_flag1">每日新品</span>
				<span class="area_dote"></span>
				<span class="area_flag2">品质保证</span>
				<span class="area_flag3">
					<form method="get" action="" id="post_file">
						<input id="keyword" name="keyword" placeholder="在此输入您要找的宝贝！" type="text"/>
						<input type="hidden" id="file_search" value="1" />
						<input id="sub" type="submit" value=""/>
					</form>
				</span>
			</div>
		</div>
		<div class="mainnav">
		<div class="main_menu">
			<ul>
	            <li><a href="./asp.php">首页</a></li>
	            <!--<li><a href="./remai.php">热卖单品</a></li>-->
	            <li style="position:relative;">
	            	<img style="position:absolute;top:-3px;right:-10px;" src="./images/asp/hot.png">
	            	<a href="./tejia.php">天天特价</a>
	            </li>
	            <li><a href="./shop.php">店铺推荐</a></li>
	            <li><a target="_blank" href="https://qiang.taobao.com/ ">淘抢购</a></li>
	            <li><a target="_blank" href="https://headline.taobao.com/feed/feedList.htm?spm=a21bo.50862.226762.1.3DPJrz">上头条</a></li>
	            <!--<li><a href="">店铺推荐</a></li>-->
	        </ul>
		</div>
		</div>
	</div>