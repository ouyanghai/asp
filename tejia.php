<?php include_once( "./header.php" ); ?>
<!--用于关键字搜索-->
<input id="file_path" type="hidden" value="3"/>
<link rel="stylesheet" type="text/css" href="./css/tejia.css">
<!-- 淘宝广告start -->
<script type="text/javascript" src = "http://www.niurj.com/js/taobao.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.niurj.com/css/taobao.css">
<div id="poster" style="z-index:99999;">
    <div class="post-title">
        <span>淘宝热卖商品推荐</span>      
        <a href="javascript:;"></a>
    </div>
    <div>
        <script type="text/javascript" src="http://i.haodianpu.com/detail.php?aid=59&param=360*160"></script>  
    </div>
</div>
<!-- 淘宝广告end -->

<div class="content">
    <div class="items-cid">
        <div class="banner_cid">
        <dl>
            <dd>
                <a href="./tejia.php?promotionId=0">
                    <span style="line-height:60px;height:60px;">全部</span>
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=1">
                    <span>时尚女装</span>
                    <i class="ct-icon ct-icon-nvzhuang"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=2">
                    <span>品质男装</span>
                    <i class="ct-icon ct-icon-nanzhuang"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=3">
                    <span>男鞋女鞋</span>
                    <i class="ct-icon ct-icon-xiezi"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=4">
                    <span>包包配饰</span>
                    <i class="ct-icon ct-icon-xiangbao"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=9">
                    <span>母婴儿童</span>
                    <i class="ct-icon ct-icon-muying"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=5">
                    <span>美容护肤</span>
                    <i class="ct-icon ct-icon-meizhuang"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=7">
                    <span>日用百货</span>
                    <i class="ct-icon ct-icon-jujia"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=10">
                    <span>车品户外</span>
                    <i class="ct-icon ct-icon-jiafang"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=8">
                    <span>美食特产</span>
                    <i class="ct-icon ct-icon-meishi"></i> 
                </a>
            </dd>
            <dd>
                <a href="./tejia.php?promotionId=6">
                    <span>数码家电</span>
                    <i class="ct-icon ct-icon-shuma"></i> 
                </a>
            </dd>
            <dd class="border-none">
                <a href="./tejia.php?promotionId=11">
                    <span>舒适内衣</span>
                    <i class="ct-icon ct-icon-neiyi"></i> 
                </a>
            </dd>
            <dd class="border-none">
                <a href="./tejia.php?promotionId=12">
                    <span>其他</span>
                    <i class="ct-icon-qita"></i> 
                </a>
            </dd>
        </dl>
    </div>
    </div>
    <div class="today-items"></div>
    <div class="page-items">
        <div id="nothing_tip" style="display:none;width:200px;text-align:center;margin:0 auto;">没有相应结果</div>
        <div class="tcdPageCode"></div>
    </div>
</div>

<script type="tpl" id="template">
    <dl class="item-row">
        <div class="items-flag"></div>
        <a href="http://item.taobao.com/item.htm?id={{iid}}" target="_blank">
        <dt class="item-row-pic"><img src="{{pic_url}}" title="{{title}}"></dt>
        <dd class="item-row-desc" title="{{title}}">{{title}}</dd>
        </a>
        <dd class="item-row-price" title="{{price}}"><span>{{price}}</span></dd>
        <a href="http://item.taobao.com/item.htm?id={{iid}}" target="_blank">
        <dd class="item-row-flag" title="淘宝">
                <i></i>
                <span>淘宝</span>
        </dd>
        </a>
    </dl>
</script>
<?php 
$page=!empty($_GET['page'])?$_GET['page']:1; 
$cid = isset($_GET['promotionId']) ? $_GET['promotionId'] : 0;
$t_page = !empty($_GET['keyword'])?1:10;
$keyword = !empty($_GET['keyword'])?$_GET['keyword']:'';
?>
<script type="text/javascript">

$(function(){
    var s = document.createElement( 'script' );
    s.src = "/api.php?tpos=1&page=<?php echo $page; ?>&cid=<?php echo $cid ?>&keyword=<?php echo $keyword; ?>&callback=shop&ts=" + (+new Date());
    document.body.appendChild( s );
});
function shop(json){
    if (json.items) {
        for (var i in json.items) {
            var html = $("#template").html();
            for (var j in json.items[i]) {
                reg = "/{{" + j + "}}/ig";
                html = html.replace( eval( reg ), json.items[i][j] );
            }
            $(".today-items").append(html);
        }
    }else{
        $("#nothing_tip").show();
    }
    $(".tcdPageCode").createPage({
        pageCount:<?php echo $t_page;?>,
        current:<?php echo $page; ?>,
        backFn:function(p){
            location.href = "./tejia.php?page="+p;
        }
    });
}
</script>
<?php include_once( "./footer.php" ); ?>