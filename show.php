<?php
$refer = isset($_GET['refer']) ? $_GET['refer'] : ( isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "" );
$from = isset($_GET['from']) ? $_GET['from'] : "";
$id = isset($_GET['id']) ? $_GET['id'] : "";
$sid = isset($_GET['sid']) ? $_GET['sid'] : "";
$iid = isset($_GET['iid']) ? $_GET['iid'] : "";
$forward = "";
/*
if( empty($_GET["ll"]) ){    
    $url = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
    $url .= ( strstr($url, "?") == "" ? "?" : "&" ) . "ll=3939044495&refer=".urlencode($refer);
    echo "<html><head><meta http-equiv='refresh' content='5; url={$url}' /></head><body><a id='link' href='{$url}' target='_self'></a><script language='javascript'>if( self != top ){top.location.href=self.location.href;}else{setTimeout(function(){self.location='{$url}';}, 3000);document.getElementById('link').click();}</script></body></html>";
    exit;
}
*/
if( $from == "tbd" ){
    $url = parse_url($refer);
    $query = isset($url['query']) ? $url['query'] : "";
    parse_str($query, $params);

    $word = "";
    isset($params['wd']) && $word = urldecode($params['wd']);
    isset($params['word']) && $word = urldecode($params['word']);

    if( empty($id) ){
        // 去掉下面四行则随机显示店铺
        if( empty($word) ){
            require_once("./tejia.php");
            exit;
        }

        $url = "http://i.haodianpu.com/best/bdtg/shoptongji?wd={$word}&rdm=".time();
    }
    else{
        $url = "http://i.haodianpu.com/best/bdtg/tongji?id={$id}&wd={$word}&rdm=".time();
    }

    $route = @file_get_contents( $url );
    empty($route) && $route = "http://zhaoqian.haodianpu.com/";
    $route .= ( strstr($route, "?") == "" ? "?" : "&" ) . "from=tg"; 
    if( strstr($route, "shop") ) {            
        if( preg_match('/shop([\d]{5,12})\.taobao\.com/', $route, $match)) {
            $sid = $match[1];
        }
    }
    if( strstr($route, "item.taobao.com") ) {          
        if( preg_match('/id=([\d]{9,12})\S+(?:\.0|&sid=(\d{6,12}))/', $route, $match)) {
            $iid = $match[1];
			$sid = !empty($match[2]) ? $match[2] : '';
        }
    }  
    if( !empty($refer) && preg_match("/(www\.baidu\.com|taobao\.com)/i", $refer) ){
        $ip = !empty($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : ( !empty($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : ( !empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "127.0.0.1" ) );
        $url = "http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
        $content = @file_get_contents( $url );
        if( !empty($content) && substr($content, 0, 1) == "{" && substr($content, -1, 1) == "}" ){
            $json = json_decode($content);
            if( $json->code == 0 && $json->data->city != "\u5317\u4eac\u5e02" ){
                $forward = $route;
            }
        }
    }
}

 
if ( empty($sid) && empty($iid) ) {    
    include_once("./tejia.php");
    exit;
}

if( $from == "remai"){
    $route = "http://shop{$sid}.taobao.com";
}

include_once( "./header.php" );
?>

<div id="show-item" class="container">
    <div class="item-detail">
        <h3><a href="" target="_blank" id="item-title"></a></h3>
        <div class="clearfix">
            <div class="item-pic">
                <a href="" target="_blank"><img src=""></a>
            </div>
            
            <div class="item-info">
                <div class="item-buy">
                    <i>¥</i>
                    <span class="item-price"></span>
                    <a class="pte_buy" href="" target="_blank"></a>
                </div>
                
                <div style="margin:5px 30px; text-align:center;">
                    <!-- JiaThis Button BEGIN -->
                    <div class="jiathis_style_32x32">
                        <a class="jiathis_button_qzone"></a>
                        <a class="jiathis_button_tsina"></a>
                        <a class="jiathis_button_tqq"></a>
                        <a class="jiathis_button_weixin"></a>
                        <a class="jiathis_button_renren"></a>
                        <a href="http://www.jiathis.com/share" class="jiathis jiathis_txt jtico jtico_jiathis" target="_blank"></a>
                    </div>
                    <script type="text/javascript" src="http://v3.jiathis.com/code_mini/jia.js?uid=1373890231172857" charset="utf-8"></script>
                    <!-- JiaThis Button END -->
                </div>
            </div>
        </div>
    </div>
    
    <div id="item-description"></div>
</div>

<div id="show-shop" class="container">
    <div class="clearfix" style="height:120px;">
        <div class="shop-pic">
            <a href="" target="_blank"><img src=""></a>
        </div>
        <div class="shop-info">
            <div class="shop-detail">
                <p><a href="" target="_blank"><span class="detail1" style="color:red;font-size:15px;font-weight:bold"></span></a></p>
                <p><strong>店铺简介：</strong><span class="detail2"></span></p>
                <p><strong>综合评分：</strong><span class="detail4">4.8</span></p>
                <p><strong>开店时间：</strong><span class="detail3"></span></p>
            </div>
             <!-- JiaThis Button BEGIN -->
            <div class="jiathis_style_32x32" style="float:right;margin-top:50px;margin-right:40px;">
                <a href="" target="_blank" id="shop-title" style="float:left;margin-right:5px;width:127px;height:36px;background:url('http://img03.taobaocdn.com/imgextra/i3/62192401/T2RzaxXodbXXXXXXXX_!!62192401.gif') no-repeat 95% center;display:block;"></a>
                <a class="jiathis_button_qzone"></a>
                <a class="jiathis_button_tsina"></a>
                <a class="jiathis_button_weixin"></a>
            </div>
            <script type="text/javascript" src="http://v3.jiathis.com/code_mini/jia.js?uid=1373890231172857" charset="utf-8"></script>
            <!-- JiaThis Button END -->
        </div>

    </div>
    
</div>
<div style="width:980px;margin:0 auto;margin-top:20px;height:32px;color:#ff6600;"><span style="font-size:20px;float:left;"><img src="./images/asp/remai01.png"></span></div>
<!--店铺宝贝展示-->
<div class="shop-items">
</div>

<script type="tpl" id="template">
    <dl class="item-row">
        <div class="items-flag"></div>
        <a href="http://item.taobao.com/item.htm?id={{iid}}" target="_blank">
        <dt class="item-row-pic"><img src="{{pic_url}}" title="{{title}}"></dt>
        <dd class="item-row-desc" title="{{title}}">{{title}}</dd>
        <dd class="item-row-price" title="{{price}}"><span>{{price}}</span></dd>
        </a>
    </dl>
</script>

<script type="text/javascript">

$(function(){
    var s = document.createElement( 'script' );    
    
    <?php if(!empty($sid) && empty($iid)) { ?>
        s.src = "/api.php?sid=<?php echo $sid; ?>&callback=shop&ts=" + (+new Date());
    <?php 
	} else if(!empty($sid) && !empty($iid)) { 
		$from = (!empty($from) && $from=='tbd') ? '&from=tbd' : '';
	?>
        s.src = "/api.php?sid=<?php echo $sid; ?>&callback=shop&ts=" + (+new Date());
        //s.src = "/api.php?iid=<?php echo $iid; ?>&sid=<?php echo $sid;?>&callback=show<?php echo $from;?>&ts=" + (+new Date());    
    <?php } ?>    
    
    document.body.appendChild( s );
    
    <?php if( !empty($forward) ) { ?>
    //setTimeout(function(){self.location="<?php echo $forward;?>";}, 5000);
    <?php } ?>   
});

// 展示宝贝
function show(json){

    json.detail_url = json.detail_url + ( json.detail_url.search(/\?/) > -1 ? "&" : "?" ) + "from=tg";    
    $("#show-item").show();
    $("#item-title").text( json.title ).attr( "href", json.detail_url );
    $(".item-pic").find("a").attr("href", json.detail_url);
    if (json.pic_url != '') {
        $(".item-pic").find("img").attr("src", json.pic_url);
    }
    $(".item-price").text( json.price );
    $("#item-description").html( json.desc );

    $(".pte_buy").attr("href", json.detail_url).hide();

    $.merge($("#show-item a"), $("#show-item area")).each(function(){ $(this).attr("href", "#").attr("target", "_self"); });
    
	document.title = json.title;
}

// 展示店铺信息
function shop(json){
    json.url = json.url + ( json.url.search(/\?/) > -1 ? "&" : "?" ) + "from=tg";
    $("#show-shop").show();
    $("#shop-title").attr("href", "<?php echo $route; ?>");
    $(".shop-pic").find("a").attr("href", json.url);
    $(".shop-detail").find("a").attr("href", json.url);
    json.pic_path = json.pic_path.replace(" ", "");
    if (json.pic_path != "") {
        $(".shop-pic").find("img").attr("src", 'http://logo.taobao.com/shop-logo'+json.pic_path);
    }
    $(".shop-detail .detail1").text(json.title);
    $(".shop-detail .detail2").html(json.desc);
    $(".shop-detail .detail3").text(json.created);
    var _height = parseInt($(".shop-info").height()+30);
    if (_height>300) {
        $("#show-shop").height(_height);
    }
    
    if (json.items) {
        for (var i in json.items) {
            var html = $("#template").html();
            for (var j in json.items[i]) {
                reg = "/{{" + j + "}}/ig";
                html = html.replace( eval( reg ), json.items[i][j] );
            }
            $(".shop-items").append(html);
        }
    }
}
</script>
<?php include_once("./footer.php"); ?>